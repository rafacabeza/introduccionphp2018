<?php
//aprobechamos para ver un par de funciones para el manejo de variables.
//isset comprueba si una variable está definida y no es null
//empty comprueba si una variable contiene algún valor distinto de 0 o false
echo '<meta charset="UTF-8">';
if(isset($_POST) && !empty($_POST)){
    echo "Recibido!! Mira la URL en tu navegador. Está 'limpia' <hr>";
    echo "Bienvenido $_POST[nombre]. ";
    echo "Aunque me ha dicho un pajarito que también es conocido como $_REQUEST[hide]";
    echo "ojo!! en el hidden hay una HTML injection";
    //var_dump nos puede ayudar a entender lo que hemos recibido
    echo "<pre>";
    var_dump($_POST);
    echo "</pre>";

}
 else {
     echo "No hemos recibido nada!";
}
