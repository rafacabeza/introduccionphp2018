<div id="main">

<h2>Variables y tipos </h2>
<p>
    Los <strong>tipos básicos</strong> son:
</p>
<ul>
    <li>Entero: número entero con signo </li>
    <li>Flotante: número decimal con signo </li>
    <li>Booleano: vale true o false </li>
    <li>Cadena de caracteres: cadena de caracteres delimitada por
        comillas. Las comillas simples
        interpretan el texto literalmente, mientras que las dobles
        sustituyen las variables.</li>                </ul>

<p>Los identicadores de <strong>variables</strong> son cadenas alfanumericas precedididas
    del carácter "$". Se usan sin declaración previa.</p>

<p>Para <strong>comentarios</strong> de unas sólo línea (o parte de ella) se pueden usar
    indistintamente // o #.

    Si se desean comentario de varias líneas, se abren con /* y se cierran con */
</p>

<p>Se imprimen con echo o print:</p>
<?php Format::ejemplo(2);?>


<h3>Ámbito de variables</h3>

<ul>
    <li>
    Las variables declaradas a nivel de script son globales. Accesibles desde cualquier lugar.
    </li>
    <li>
    Las variables definidas dentro de una función son variables privadas y por tanto sólo accesibles desde dentro de la propia función.
    </li>
    <li>
    Las variables declaradas dentro de una clase son atributos y sólo pueden ser accedidas a través de la propia clase o de objetos de la misma.
    </li>1
</ul>
<h3>Variables superglobales</h3>

</div>
